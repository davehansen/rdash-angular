angular.module('RDash')
    .controller('MasterCtrl', ['$scope', '$cookieStore', '$http', MasterCtrl]);

function MasterCtrl($scope, $cookieStore, $http) {
    var mobileView = 992;
    $scope.messages = [];

    $scope.getWidth = function() {
        return window.innerWidth;
    };

    $scope.$watch($scope.getWidth, function(newValue, oldValue) {
        if (newValue >= mobileView) {
            if (angular.isDefined($cookieStore.get('toggle'))) {
                $scope.toggle = ! $cookieStore.get('toggle') ? false : true;
            } else {
                $scope.toggle = true;
            }
        } else {
            $scope.toggle = false;
        }
    });

    $scope.registeredUsers = $scope.pendingDelivery = $scope.sentPastHour = $scope.sentPastDay = '--';

	$http.get('/api/v1/info/users').then(function(response) {
		// TODO remove loading classes
        $scope.registeredUsers = response.data['users']
	});

	$http.get('/api/v1/info/hourly').then(function(response) {
		// TODO remove loading classes
        $scope.sentPastHour = response.data['hourly']
	});

	$http.get('/api/v1/info/daily').then(function(response) {
		// TODO remove loading classes
        $scope.sentPastDay = response.data['daily']
	});

    $scope.getHistory = function() {
        // TODO XXX insert loading tag `#chat-history-body rd-loading`
        var currentTime = new Date().getTime();
        $http.post('/api/v1/chat/history', {"command": "history", "client_time": currentTime, "since": (currentTime - 86400000)}).then(function(response) {
            // TODO remove loading tag `#chat-history-body rd-loading`
            $scope.messages = response.data;
        });
    };

    $scope.sendMessage = function() {
        var currentTime = new Date().getTime();
        $http.post('/api/v1/chat/send', {"sender": 'dashboard-test', "msg": "boom", "client_time": currentTime}).then(function(response) {
            // TODO remove loading tag `#chat-history-body rd-loading`
            // $scope.messages = response.data;

        });
    };

    window.onresize = function() {
        $scope.$apply();
    };
}
